using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    public void ThreeCups()
    {
        StartCoroutine(WaitThree());
    }
    public void FourCups()
    {
        StartCoroutine(WaitFour());
        SceneManager.LoadScene("4Cups");
    }
    public void FiveCups()
    {
        StartCoroutine(WaitFive());
        SceneManager.LoadScene("5Cups");
    }

    IEnumerator WaitThree()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("3Cups");
    }
    IEnumerator WaitFour()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("4Cups");
    }
    IEnumerator WaitFive()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("5Cups");
    }
}
