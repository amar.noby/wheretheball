using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndScore : MonoBehaviour
{
    TextMeshProUGUI endScore;
    // Start is called before the first frame update
    void Start()
    {
        endScore = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        endScore.text = Score.score.ToString();
    }
}
