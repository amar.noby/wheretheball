using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CupBehaviour : MonoBehaviour
{
    public GameManager manager;
    Animator animator;
    public Button button;
    public GameObject DimPanel;
    public GameObject LeaderboardPanel;
    bool firstDone = false;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Check()
    {
        if (gameObject.name == "BallCup")
        {
            manager.enabled = true;
            Score.score += 15;
        }
        else
        {
            if (firstDone)
            {
                DimPanel.SetActive(true);
                LeaderboardPanel.SetActive(true);
            }
            firstDone = true;
        }
    }
    public void PlayAnimation()
    {
        animator.Play("CupAnimation", 0, 0f);
        StartCoroutine(ButtonInactive());
    }
    IEnumerator ButtonInactive()
    {
        button.enabled = false;
        yield return new WaitForSeconds(2);
        button.enabled = true;
    }
}
