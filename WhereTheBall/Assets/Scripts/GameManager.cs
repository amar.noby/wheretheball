using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Transform[] Cups;
    public Transform[] Positions;
    public int numOfMovements;
    public float speed;
    int random1;
    int random2;
    bool infinite = true;
    int movesDone;
    public Animator[] CupAnimators;
    public AudioSource audioSource;
    public AudioClip switchSound;
    public GameObject[] Buttons;
    // Start is1 called before the first frame update
    void Start()
    {
        Randomize();
    }

    // Update is called once per frame
    void Update()
    {
        ChangePositions();
    }
    void ChangePositions()
    {
        for(int i = 0; i < CupAnimators.Length; i++)
        {
            CupAnimators[i].enabled = false;
            Buttons[i].SetActive(false);
        }
        for (int i = 0; i < numOfMovements; i++)
        {
            Cups[random1].position = Vector3.MoveTowards(Cups[random1].position, Positions[random2].position, speed * Time.deltaTime);
            Cups[random2].position = Vector3.MoveTowards(Cups[random2].position, Positions[random1].position, speed * Time.deltaTime);

            if (Cups[random1].position == Positions[random2].position && Cups[random2].position == Positions[random1].position)
            {
                movesDone++;
                Vector3 temp;
                audioSource.PlayOneShot(switchSound);
                temp = Positions[random1].position;
                Positions[random1].position = Positions[random2].position;
                Positions[random2].position = temp;
                Randomize();  
            }
        }
        if(movesDone == numOfMovements)
        {
            this.enabled = false;
            movesDone = 0;
            numOfMovements++;
            speed += 0.1f;
            for (int i = 0; i < CupAnimators.Length; i++)
            {
                CupAnimators[i].enabled = true;
                Buttons[i].SetActive(true);

            }
            for (int j = 0; j < Cups.Length; j++)
            {
                Cups[j].position = Positions[FixPositions(j)].position;
            }
        }
    }
    void Randomize()
    {
        while (infinite)
        {
            random1 = Random.Range(0, Cups.Length);
            random2 = Random.Range(0, Cups.Length);
            if (random1 != random2)
            {
                break;
            }
        }
    }
    int FixPositions(int cupIndex)
    {
        float temp = float.PositiveInfinity;
        int index = 0;
        float distance;
        for (int i = 0; i < Cups.Length; i++)
        {
            distance = Vector2.Distance(Cups[cupIndex].position, Positions[i].position);
            if (distance <= temp)
            {
                index = i;
                temp = distance;
            }
        }
        return index;
    }
 }
