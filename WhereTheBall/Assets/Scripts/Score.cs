using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    TextMeshProUGUI ScoreText;
    public static int score;
    // Start is called before the first frame update
    void Start()
    {
        ScoreText = GetComponent<TextMeshProUGUI>();
        score = -15;
    }

    // Update is called once per frame
    void Update()
    {
        if(score >= 0)
        {
            ScoreText.text = score.ToString();
        }
    }
}
