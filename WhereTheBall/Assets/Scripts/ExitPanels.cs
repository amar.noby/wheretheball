using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitPanels : MonoBehaviour
{
    public GameObject DimPanel;
    public GameObject SettingsPanel;
    public GameObject LevelPanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && SettingsPanel.activeInHierarchy)
        {
            SettingsPanel.SetActive(false);
            DimPanel.SetActive(false);
        }
        if(Input.GetKeyDown(KeyCode.Escape) && LevelPanel.activeInHierarchy)
        {
            LevelPanel.SetActive(false);
            DimPanel.SetActive(false);
        }

    }
}
